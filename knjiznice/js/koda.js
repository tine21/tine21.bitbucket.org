/* global L, distance */



// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
var mapa;
var layer;
window.addEventListener('load',function(){
  
  const FRI_LAT = 46.05004;
const FRI_LNG = 14.46931;

  
  var mapOptions = {
    center: [FRI_LAT, FRI_LNG],
    zoom: 12
    // maxZoom: 3
  };
  
     mapa= new L.map('mapa_id',mapOptions);
     layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
    mapa.addLayer(layer);
    
    var podatki = pridobiPodatke(function(jsonRezultat){
     izrisBolnisnic(jsonRezultat);
    });
    
      // Objekt oblačka markerja
  var popup = L.popup();

  function obKlikuNaMapo(e) {
    var latlng = e.latlng;
    popup
      .setLatLng(latlng)
      .setContent("Izbrana točka:" + latlng.toString())
      .openOn(mapa);
    //console.log(latlng);
   // prikazPoti(latlng);
  }

 // mapa.on('click', obKlikuNaMapo);
  
});




function pridobiPodatke(callback) {      
 

  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open("GET", "https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json", true);
  xobj.onreadystatechange = function () {
    // rezultat ob uspešno prebrani datoteki
    if (xobj.readyState == 4 && xobj.status == "200") {
        var json = JSON.parse(xobj.responseText);
        // vrnemo rezultat
       callback(json);
    }
  };
  xobj.send(null);
}

function izrisBolnisnic(jsonRezultat){
  var znacilnosti = jsonRezultat.features;
  var latLngs;
  
  for(var i = 0; i< znacilnosti.length; i++ ){
      var opis ="<div>";
      if(znacilnosti[i].geometry.type=="Polygon"){
        latLngs = znacilnosti[i].geometry.coordinates;
        if(znacilnosti[i].properties.name){
          opis += "Naziv: " + znacilnosti[i].properties.name + "</br>";
        }
        if(znacilnosti[i].properties["addr:city"])
        {
          opis += "Naslov: " + znacilnosti[i].properties["addr:city"];
          if(znacilnosti[i].properties["addr:street"]){
            opis+= ", " + znacilnosti[i].properties["addr:street"]+ " " + znacilnosti[i].properties["addr:housenumber"];
          }
        }
        if(opis == "<div>"){
          opis += "Za to bolnico ni podatkov.";
        }
                                                             //"<div>Naziv: " + opis + "</div>"
        opis += "</div>";
        izrisPolygona(latLngs,opis);
      }
  }
  
}


function izrisPolygona(latLngs, opis){
  var koordinate;
  //console.log(latLngs[0].length);
  koordinate =  switchCoords(latLngs[0]);
  
  var polyline = L.polyline(koordinate, {color: 'blue'});
  var polygon = L.polygon(koordinate, {color: 'blue'}).bindPopup(opis);
  //polyline.bindPopup("<div>Naziv: " + opis + "</div>").openPopup();
  polygon.addTo(mapa);
  polyline.addTo(mapa);
 
//var polyline = L.polyline(latlngs, {color: 'red'}).addTo(mapa);
//mapa.fitBounds(polyline.getBounds());
  
}

function switchCoords(coords){
  var a;
  for(var i = 0; i< coords.length;i++){
    var a = coords[i][0];
    coords[i][0] =coords[i][1];
    coords[i][1] = a; 
  }
  return coords;
}
